package View;

import Controller.ControlOutput;

import static Controller.ControlOutput.onStop;

import java.util.Scanner;

public class Menu {

    private int userInputMainMenu;
    private int userInputLastMenu;
    public static final Scanner sc = new Scanner(System.in);

    public int getUserInputMainMenu() {
        return userInputMainMenu;
    }

    public void setUserInputMainMenu(int userInputMainMenu) {
        this.userInputMainMenu = userInputMainMenu;
    }

    public int getUserInputLastMenu() {
        return userInputLastMenu;
    }

    public void setUserInputLastMenu(int userInputLastMenu) {
        this.userInputLastMenu = userInputLastMenu;
    }

    public void mainMenu() {
        System.out.println("""
                ----------------------------------------------------------
                Aplikasi Pengolah Nilai Siswa
                ----------------------------------------------------------
                """);
        System.out.println("Letakan file csv dengan nama file data_sekolah di direktori berikut: C://temp/direktori \n");
        System.out.println("pilih menu:");
        System.out.println("1. Generate txt untuk menampilkan modus");
        System.out.println("2. Generate txt untuk menampilkan modus, mean, median");
        System.out.println("3. Generate txt kedua File");
        System.out.println("0. Exit");
        System.out.print(">>> ");
        userInputMainMenu = sc.nextInt();
    }

    public void lastMenu() {
        System.out.println("0. Exit");
        System.out.println("1. Kembali Kemenu utama");
        System.out.print("Pilih Menu : ");
        userInputLastMenu = sc.nextInt();
        footer();
    }

    public void footer() {
        if (userInputMainMenu == 0 || userInputLastMenu == 0) {
            System.out.println("""
                            Thank Youuuuu ^-^
                    """);
            onStop();
        } else if (userInputLastMenu == 1) {
            ControlOutput.setKondisi(true);
        } else {
            System.out.println("Salah Input Pilihan!!");
        }
    }
}
