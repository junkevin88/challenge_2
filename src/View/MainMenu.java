package View;
import java.io.IOException;
import static Controller.ControlOutput.onStart;

public class MainMenu {
    public static void main(String[] args) {
        try {
            onStart();
        }catch (NullPointerException | IOException e){
            System.out.println(e.getMessage());
        }
    }
}
