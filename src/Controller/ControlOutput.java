package Controller;

import Service.Logic;
import static Service.ResultLogic.*;
import java.io.IOException;
import java.util.InputMismatchException;
import View.Menu;
import static Utility.PathInputOutput.fileReader;
import static Utility.PathInputOutput.fileWriterFrequencies;
import static Utility.PathInputOutput.fileWriterMMM;
import static Utility.Enum.CSV;
import static Utility.Enum.TXT;




public class ControlOutput {
        private static boolean kondisi = true;
        public static void setKondisi(boolean kondisi) {
            ControlOutput.kondisi = kondisi;
        }
        private static final Menu menu = new Menu();
        private static final IO ioFile = new IO();

        public static void onStart() throws InputMismatchException, IOException {
            Logic logic = new Logic(ioFile.read(fileReader,CSV.name().toLowerCase()));
            while (kondisi) {
                try {
                    menu.mainMenu();
                    switch (menu.getUserInputMainMenu()) {
                        case 0 -> menu.footer();
                        case 1 -> {
                            ioFile.write(resultStringMenu1(logic), fileWriterFrequencies,TXT.name().toLowerCase());
                            menu.lastMenu();
                        }
                        case 2 -> {
                            ioFile.write(resultStringMenu2(logic), fileWriterMMM,TXT.name().toLowerCase());
                            menu.lastMenu();
                        }
                        case 3 -> {
                            ioFile.write(resultStringMenu1(logic), fileWriterFrequencies,TXT.name().toLowerCase());
                            ioFile.write(resultStringMenu2(logic), fileWriterMMM,TXT.name().toLowerCase());
                            menu.lastMenu();
                        }
                        default -> System.out.println("Salah Input Pilihan!!");
                    }
                }catch (InputMismatchException e){
                    System.out.println("Tolong Masukkan Angka!!");
                    Menu.sc.next();
                }
            }
        }

        public static void onStop(){
            kondisi = false;
            System.exit(0);
        }
    }