package Controller;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface InterfaceIO {
    Map<String, List<Integer>> read(String path, String extension) throws IOException;
    void write(String wantWrite, String path, String extension)throws IOException;
    void success(String url);
    void failed();
}
