package Service;

import java.util.List;
import java.util.Map;

public interface InterfaceLogic {
    List<Map<Integer,Integer>> modusFrekuensi();
    List<Double> mean();
    List<Double> median();
    List<Integer> modus();
}
