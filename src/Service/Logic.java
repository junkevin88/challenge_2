package Service;

import Controller.InterfaceIO;
import Repository.InputData;

import java.util.*;

public class Logic extends InputData implements InterfaceLogic {
    public Logic(Map<String, List<Integer>> dataNilai) throws NullPointerException {
        super(dataNilai);

        if (dataNilai.isEmpty()) {
            throw new NullPointerException("File tidak ditemukan");
        } else {
            mean();
            median();
            modusFrekuensi();
            modus();
        }
    }

    @Override
    public List<Double> mean() {
        List<Double> tempMean = new ArrayList<>();
        super.dataNilai.entrySet().stream()
                .map(keys -> keys.getValue())
                .forEach(integers -> {
                    List<Integer> nilai = new ArrayList<>(integers);
                    double temp = nilai.stream()
                            .mapToDouble(value -> value)
                            .average().orElse(Double.NaN);
                    tempMean.add(temp);
                });
        super.setHasilMean(tempMean);
        return super.getHasilMean();
    }

    @Override
    public List<Double> median() {
        List<Double> tempMedian = new ArrayList<>();
        super.dataNilai.entrySet().stream()
                .map(keys -> keys.getValue())
                .forEach(integers -> {
                    List<Integer> nilai = new ArrayList<>(integers);
                    double size = nilai.stream().mapToDouble(value -> value).count();
                    double temp = (size % 2 == 1) ?
                            nilai.get((int) (size / 2)) : (nilai.get((int) (size / 2 + 1)) + nilai.get((int) (size / 2))) / 2;
                    tempMedian.add(temp);
                });
        super.setHasilMedian(tempMedian);
        return super.getHasilMedian();
    }

    @Override
    public List<Integer> modus() {
        List<Integer> tempModus = new ArrayList<>();
        Set<Map.Entry<String, List<Integer>>> entries = super.dataNilai.entrySet();
        int result = 0;
        int h = 0;
        int j = 0;
        int max = 0;
        for (var entry : entries) {
            List<Integer> perKelas = new ArrayList<>(super.dataNilai.get(entry.getKey()));
            int[] dataArray = new int[perKelas.size()];
            for (int i = 0; i < perKelas.size(); i++) {
                dataArray[i] = perKelas.get(i);
            }
            for (int i :
                    dataArray) {
                if (i == j) {
                    h++;
                    if (h > max) {
                        max = h;
                        result = j;
                    }
                } else {
                    j = i;
                    h = 1;
                }
            }
            tempModus.add(result);
        }
        super.setHasilModus(tempModus);
        return super.getHasilModus();
    }

    @Override
    public List<Map<Integer, Integer>> modusFrekuensi() {
        List<Map<Integer, Integer>> modusfrekuensi = new ArrayList<>();
        super.dataNilai.entrySet().stream()
                .map(keys -> keys.getValue())
                .forEach(integers -> {
                    HashMap<Integer, Integer> frequency = new HashMap<>();
                    List<Integer> nilai = new ArrayList<>(integers);
                    nilai.stream().forEach(integer -> {
                        frequency.merge(integer, 1, Integer::sum);
                    });
                    modusfrekuensi.add(frequency);
                });
        super.setHasilFrekuensi(modusfrekuensi);
        return super.getHasilFrekuensi();
    }
}
