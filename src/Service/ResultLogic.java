package Service;

import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class ResultLogic {

    private static StringBuilder stringBuilder;

    public static String resultStringMenu1(Logic logic){
        Optional<Logic> cs = Optional.ofNullable(logic);
        stringBuilder = new StringBuilder();
        if (cs.isPresent()) {
            stringBuilder.append("Berikut Hasil Pengolahan Nilai\n\n");
            for (int i = 0; i < logic.getKelas().size(); i++) {
                stringBuilder.append(logic.getKelas().get(i)).append(" : \n");
                stringBuilder.append("-----------------------------\n");
                stringBuilder.append("Nilai           | Frekuensi\n");
                stringBuilder.append("-----------------------------\n");
                Map<Integer, Integer> temp = logic.getHasilFrekuensi().get(i);
                Set<Map.Entry<Integer, Integer>> entries = temp.entrySet();
                int lessThanSix = 0;
                for (var entry : entries) {
                    if (entry.getKey() < 6) lessThanSix += entry.getValue();
                }
                stringBuilder.append(String.format("Kurang dari 6   | %-10d", lessThanSix)).append("\n");
                for (var entry : entries) {
                    if (entry.getKey() >= 6)
                        stringBuilder.append(String.format("%-15d | %-10d", entry.getKey(), entry.getValue())).append("\n");
                }
                stringBuilder.append("-----------------------------\n\n");
            }
        }else {
            stringBuilder.append("Error, Data Kosong!!");
        }
        return stringBuilder.toString();
    }
    public static String resultStringMenu2(Logic logic){
        Optional<Logic> cs = Optional.ofNullable(logic);
        stringBuilder = new StringBuilder();
        if (cs.isPresent()) {
            stringBuilder = new StringBuilder();
            stringBuilder.append("Berikut Hasil Pengolahan Nilai\n\n");
            stringBuilder.append("----------------------------------------\n");
            for (int i = 0; i < logic.getKelas().size(); i++) {
                stringBuilder.append(logic.getKelas().get(i)).append("\n");
                stringBuilder.append("Mean   : ").append(String.format("%.2f", logic.getHasilMean().get(i))).append("\n");
                stringBuilder.append("Median : ").append(logic.getHasilMedian().get(i).toString()).append("\n");
                stringBuilder.append("Modus  : ").append(logic.getHasilModus().get(i).toString()).append("\n\n");
            }
        }else {
            stringBuilder.append("Error, Data Kosong!!");
        }
        return stringBuilder.toString();
    }
}
