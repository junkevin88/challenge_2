package Repository;

import java.util.*;

public abstract class InputData {

    private List<String> kelas;
    private List<Double> hasilMedian;
    private List<Double> hasilMean;
    private List<Integer> hasilModus;

    protected Map<String, List<Integer>> dataNilai;


    private List<Map<Integer, Integer>> hasilFrekuensi;

    public List<String> getKelas() {
        return kelas;
    }

    public void setKelas(List<String> kelas) {
        this.kelas = kelas;
    }

    public List<Double> getHasilMedian() {
        return hasilMedian;
    }

    public void setHasilMedian(List<Double> hasilMedian) {
        this.hasilMedian = hasilMedian;
    }

    public List<Double> getHasilMean() {
        return hasilMean;
    }

    public void setHasilMean(List<Double> hasilMean) {
        this.hasilMean = hasilMean;
    }

    public List<Integer> getHasilModus() {
        return hasilModus;
    }

    public void setHasilModus(List<Integer> hasilModus) {
        this.hasilModus = hasilModus;
    }

    protected InputData() {
    }

    protected InputData(Map<String, List<Integer>> dataNilai) {
        sortData(dataNilai);
        this.dataNilai = dataNilai;
        this.kelas = ambilKelas(dataNilai);
    }

    public List<Map<Integer, Integer>> getHasilFrekuensi() {
        return hasilFrekuensi;
    }

    public void setHasilFrekuensi(List<Map<Integer, Integer>> hasilFrekuensi) {
        this.hasilFrekuensi = hasilFrekuensi;
    }


    protected void sortData(Map<String, List<Integer>> dataNilai) {
        Set<Map.Entry<String, List<Integer>>> entries = dataNilai.entrySet();
        for (var daftar : entries) {
            Collections.sort(daftar.getValue());
        }
    }

    protected List<String> ambilKelas(Map<String, List<Integer>> dataNilai) {
        Set<Map.Entry<String, List<Integer>>> entries = dataNilai.entrySet();
        List<String> temp = new ArrayList<>();
        for (var daftar : entries) {
            temp.add(daftar.getKey());
        }
        return temp;
    }

}

